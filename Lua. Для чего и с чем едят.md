# Lua. Для чего и с чем едят. #
Существует удобная IDE для написания программ на языке lua - это Zero Brain Studio.
Запускаем и работаем в  IDE написанной на самой lua. Настраивается IDE также, с помощью lua конфигов.
## 1. Начало. ##
### Hello world на lua: ###
```
print("Hello world!")
```
### Вычисление факториала на языке lua: ###
```
-- defines a factorial function
function fact (n)
    if n == 0 then
        return 1
    else
        return n * fact (n-1)
    end
end


print("Enter number:")
a = io.read("*n") -- reads a number

print(fact(a))
```
### Создание переменных и блоки кода: ###
```
--Blocks

a = 1
b = a*2
a = 1;
b = a*2;
a = 1; b = a*2;
a = 1 b = a*2

print (a)
print (b)
```
Удобная функция `dofile`:
```
--Допустим, это файл библиотеки с этими функциями:

function norm (x, y)
    return (x^2 + y^2)^0.5
end

function twice (x)
    return 2*x
end

--[[

Тогда, в интерактивном режиме вы можете с помощью функции dofile
выполнить этот файл:

    > dofile("5-dofile.lua") --загрузка библиотеки
    > n = norm(3.4, 1.0)
    > print(twice(n)         --> 7.0880180...

]]--
```
### Зарезервированные слова языком Lua ###
- and
- break
- do
- else
- elseif
- end
- false
- goto
- for
- function
- if
- in
- local
- nil
- not
- or
- repeat
- return
- then
- true
- until
- while
### Глобальные переменные: ###
```
print(b) --> nil


b = 10
print(b) --> 10


b = nil
print(b) --> nil
```
## 2. Типы и значения. ##
### Виды: ###
В Lua существует 8 базовых типов:
- nil
- boolean
- number
- string
- userdata
- function
- thread
- table
Функция `type` возвращает тип для любого переданного значения
```
print(type("Hello world!")) --> string
print(type(10.4*3))         --> number
print(type(print))          --> function
print(type(type))           --> function
print(type(true))           --> boolean
print(type(nil))            --> nil
print(type(type(X)))        --> string
```
Последняя строка всегда вернет `string` вне зависимости от значения `X` так как результат функции `type` всегда является строкой.
```
print(type(a))              --> nil, а - неопределена
a = 10
print(type(a))              --> number
a = "a string"
print(type(a))              --> string
a = print                   -- да, это возможно!
a(type(a))                  --> function
```
Обратите внимание: функции являются значениями первого класса Lua. Ими можно манипулировать, как и любыми другими значениями. Подробнее об этом будет рассказано позже.
`Nil` - это тип, состоящий из одного значения `nil`.
Lua использует `nil` для обозначения отсутствующего значения.
Кроме того, глобальные переменные по умолчанию имеют значение `nil`, до своего
первого присваивания.
Также, вы можете присвоить глобальной переменной `nil`, чтобы удалить ее.
* * *
Тип `Boolean` имеет всегда два значения:
- false
- true
Lua трактует как ложь `false` и `nil`, остальные как истина.
В часности, Lua трактует `0` и `""`, как истину. Это важно помнить.
```
if 0    --> Истина
    then
        print("Истина")
    else
        print("Ложь")
    end
```
* * *
Все числа в Lua - это числа с плавающей точкой.
Нет отдельно целых чисел.
Строки в Lua - неизменяемы.
```
a = "one string"
b = string.gsub(a, "one", "another") -- Изменим часть строки

print(a)  --> one string
print(b)  --> another string

a = "hello"

print(#a) --> 5

```
Префикс '#' - называется оператором длины. Он возвращает длину строки.
```
print(#"good\0bye") --> 8
```
Длинные строки:
```
page = [[

<html>
<head>
    <title> An HTML Page</title>
</head>
<body>
    <a href="http://www.lua.org">Lua</a>
</body>
</html>

]]
```
* * *
### Приведение типов ###
```
print("10" + 1)  --> 11
print("10 + 1")  --> 10 + 1
print("-5.3e-10" * "2") --> -1.06e-009
```
### Конкатенация строк ###
Для конкатенации строк в Lua служит оператор `..`
```
print(10 .. 20)  --> 1020
```
```
line = io.read()   --прочесть строку
n = tonumber(line) --попробовать перевести ее в число

if n == nil then
    error(line .. " is not a valid number")
else
    print(n*2)
end
```
А для преобразования числа в строку вы используете функцию `tostring`.
Или конкатенировать число с пустой строкой
```
print(tostring(10) == "10")   --> true
print(10 .. "" == "10")       --> true
```
Эти преобразования всегда работают
* * *
### Таблицы. ###
Таблицы в Lua - это ассоциативный массив.
Таблицы в Lua, это не значения, ни переменные. Они объекты.
```
a = {}                  -- Создать таблицу и запомнить ссылку на нее в 'a'

k = "x"

a[k] = 10               -- Новая запись с ключом "X" и значением 10

a[20] = "great"         -- Новая запись с ключом 20 и значением "great"

print(a["x"])           --> 10

a["x"] = a["x"] + 1     -- увеличить запись "x"

print(a["x"])           --> 11
```
Таблица всегда анонимна.
```
a["x"] = 10

b = a                   -- 'b' ссылается на ту же таблицу, что и 'a'

print(b["x"])           --> 10

b["x"] = 20

print(a["x"])           --> 20

a = nil                 -- только 'b' по-прежнему ссылается на таблицу
b = nil                 -- на таблицу не осталось ссылок
```
Когда в программе больше не осталось ссылок на таблицу, сборщик мусора со временем уничтожит таблицу и переиспользует ее память
```
a = {}                  -- пустая таблица
```
Создать 1000 новых записей
```
for i = 1, 1000 do a[i] = i*2 end

print(a[9])             --> 18

a["x"] = 10

print(a["x"])           --> 10
print(a["y"])           --> nil
```
Как в видите, в последней строке, как в случае и с глобальными переменными, не инициализированое значение таблицы хранит `nil`. Это не совпадение. Lua хранит глобальные переменные в обыкновенных таблицах. Подробнее мы это рассмотрим позже.
В Lua придуман синтаксический сахар для взятия индекса
Вместо `a["name"]` вы можете написать `a.name`
```
a.x = 10
print(a.x)
print(a.y)
```
С такими разными подходами работы с объектами, новички часто путают следующие выражения:
```
a.x и a[x]
```
Ниже показана разница
```
a = {}
x = "y"
a[x] = 10           -- запись 10 в поле 'y'
print(a[x])         -- 10 - значение поля 'y'
print(a.x)          -- nil - значение поля "x" (не определено)
print(a.y)          -- 10 - значение поля 'y'
```
Чтобы представить традиционный массив или список, просто используйте таблицу с целочисленными ключами. Нет ни способа, ни необходимости объявлять размер. Вы просто инициализируете элементы, которые вам нужны.
Прочесть 10 строк, запоминая их в таблице:
```
a = {}
for i = 1, 10 do
    a[i] = io.read()
end
```
Поскольку вы можете индексировать таблицу по любому значению, вы можете начать индексы с любого числа, которое вам нравится. Однако в Lua принято начинать массивы с единицы(а не с 0 как в С), и некоторые средства Lua придерживаются этого соглашения.
Вы можете вот таким способом прочесть созданный массив используя `#`.
Работает, только для последовательностей(sequence)
```
-- print the lines
for i = 1, #a do
    print(a[i])
end
```
## Выражения ##
### Арифметические операторы ###
Все арифметические операторы такие же как и в С++
- \+ сложение
- \- вычитание
- \* умножение
- / деление
- % остаток от деления
- ^ возведение в степень
- \- унарный минус
Следующее правило определяет остаток от деления:
```
a % b == a - math.floor(a/b)*b
```
Для целочисленных операндов у него стандартное значение
Для вещественных у него есть некоторые дополнительные возможности:
`x%1` дает дробную часть `x`; `x-x%1` дает его целую часть;
Если мы хотим получить какую либо точность, то мы можем использовать следующую конструкцию
```
x = math.pi
print(x - x%0.01) --> 3.14

print(x%1)        --> 0.14159265....

print(x - x%1)    --> 3
```
* * *
### Операторы сравнения. ###
Lua предоставляет следующие операторы сравнения:
- < меньше
- \>    больше
- <=    меньше или равно
- \>=   больше или равно
- ==    равно
- ~=    не равно
```
a = {}; a.x = 1; a.y = 0
b = {}; b.x = 1; b.y = 0
c = a
```
В итоге мы получим:
`a == c`, но `a ~= b`
```
print(a==c) --> true
print(a~=b) --> true
```
* * *
### Логические операторы ###
Lua предоставляет следующие логические операторы:
- and        и
- or         или
- not        не
Оператор `and` возвращает своей первый операнд, если он ложен, иначе - второй.
Оператор `or` возвращает свой первый оператор если не ложен, иначе - второй.
```
print(4 and 5)      --> 5
print(nil and 13)   --> nil
print(false and 13) --> false
print(4 or 5)       --> 4
print(false or 5)   --> 5
```
Оператор `not` Всегда возвращает булево значение
```
print(not nil)      --> true
print(not false)    --> true
print(not 0)        --> false
print(not not 1)    --> true
print(not not nil)  --> false
```
А теперь немного о "магии" этих операторов
Полезной в Lua конструкцией является `x=x or v`, эквивалентная
```
if not x then x = v end
```
То есть, значение x устанавливается равным значением по умолчанию v, если x не определено (при условии что x не равно false)
Приведем пример такого кода:
```
x = "42" --Переменная инициализирована

x = x or "0"    --Если переменная не инициализирована, присваиваем ей ноль

print(x)

y = y or "0"

print(y)
```
Другой пример замечательной конструкции `(a and b) or c` или просто `a and b or c`
Например, с помощью ее, можно выбрать максимум из двух чисел `x` и `y`
```
max = (x>y) and x or y
```
Если по порядку рассматривать это выражение, то его легко можно понять))
* * *
### Оператор длины ###
Этот оператор не всегда очевиден. Стоит понимать, как он работает
Важно следить за "дырами" в вашем массиве
```
a = {1,2,nil,nil,3}

print(#a)   --> 5

b = {1,2,3,4,nil,nil}

print(#b)   --> 4
```
* * *
### Конструкторы таблиц ###
Стандартный конструктор:
```
days = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"}

print(days[4])  --> Wednesday
```
Инициализация таблиц по полям
```
a = {x=10,y=20}
```
Следующая строка эквивалентна предыдущей
```
a = {}
a.x = 10
a.y = 20
```
Вне зависимости от того, каким мы конструктором пользовались, мы всегда можем добавлять и удалять поля
```
w = {x=0, y=0, label="console"}
x = {math.sin(0), math.sin(1), math.sin(2)}

w[1] = "another field"      --Добавить ключ 1 к таблице w
x.f = w                     --Добавить ключ f к таблице x

print(w["x"])               --> 0
print(w[1])                 --> another field
print(x.f[1])               --> another field
w.x = nil                   --> удалить поле "x"
```
Мы можем смешивать стили инициализации
```
polyline = {
            color = "blue",
            thickness = 2,
            npoints = 4,
            {x=0, y=0}, --polyline[1]
            {x=-10, y=0},   --polyline[2]
            {x=-10, y=1},   --polyline[3]
            {x=0, y=1}  --polyline[4]
           }

print(polyline[2].x)        --> -10
print(polyline[4].y)        --> 1

```
Существует еще один ЯВНЫЙ формат создания конструкторов
```
opnames = {
            ["+"] = "add", 
            ["-"] = "sub", 
            ["*"] = "mul", 
            ["/"] = "div"
}

i = 20
s = "-"

a = {
      [i+0] = s, 
      [i+1] = s..s, 
      [i+2] = s..s..s
}

print(opnames[s])   --> sub
print(a[22])        --> ---
```
И еще абсолютно не важно чем вы отделяете элементы в таблице, запятой, или точка с запятой.
Обычно, точка с запятой используется для отделения секций...
```
arr = {1,2,3,4; r=0.2, g=0.7, b=0.1,}
```
и как видите, ничего страшного, если в конце стоит запятая
## 4. Операторы ##
### Операторы присваивания ###
Присваивание - это базовое средство изменения значений переменной или поля таблицы
```
a = "hello " .. "world"
t = {}
t.n = 0
t.n = t.n + 1

print(a)
print(t.n)
```
Луа позволяет использовать множественное присваивание
```
a, b = 10, 2*2

print(a,b)
```
В множ. присваивании луа сначала вычисляет все значения, а только потом присваивает.
Поэтому, мы можем использовать множественное присваивание для того, чтобы поменять значение двух переменных. Также, можно менять не только значения переменных, но и значения таблиц.
```
a, b = b, a

print(a,b)
```
Луа прекрасно справляется с несоответствиями с количеством присваиваний в правой и левой части выражения
```
a, b, c = 0, 1
print(a,b,c)            --> 0 1 nil
a, b = a+1, b+1, b+2    -- значение b+2 отбрасывается
print(a,b)              --> 1 2
a, b, c = 0
print(a,b,c)            --> 0 nil nil
a, b, c = 0, 0, 0
print(a,b,c)            --> 0 0 0
```
* * *
### Локальные переменные и блоки ###
Кроме глобальных переменных в луа, мы еще используем локальные переменные.
Существует ключевое слово `local`
```
j = 10          -- глобальная переменная
local i = 1     -- локальная переменная
```
В отличии от глобальной переменной, действие локальной переменной ограничено блоком, где она объявлена.
Думаю демонстрировать простые примеры не обязательно, с этим все ясно
А вот искусственные `do` блоки можно и продемонстрировать:
```
a = 1
b = 2
c = 1
do
    local a2 = 2*a
    local d = (b^2 - 4*a*c)^(1/2)
    x1 = (-b + d)/a2
    x2 = (-b - d)/a2
end -- область действия 'a2' и 'd' заканчивается здесь
print(x1,x2)
```
Хорошим стилем является использование локальных переменных где это только возможно.
Более того, доступ к локальной переменной быстрее, чем к глобальной
Распространенной идиомой в Луа является следующая:
    local foo = foo
Этот код создает локальную переменную `foo` и инициализирует ее значением глобальной переменной `foo`.
* * *
### Управляющие конструкции ###
**IF THEN ELSE ELSEIF**
```
a = 1
b = 2

if a < 0 then a = 0 end

--if a < b then return a else return b end


if a ~= b then
    print("true")
    a = 0
end

op = "/"

if op == "+" then
    r = a + b
elseif op == "-" then
    r = a - b
elseif op == "*" then
    r = a * b
elseif op == "/" then
    r = a / b
else
    error("invalid operation")
end
```
Так как в луа нет оператора свитч, то такие конструкции довольно распространенны
**WHILE**
```
a = {"1","2","3","4"}

local i = 1
while a[i] do
    print(a[i])
    i = i + 1
end
```
**REPEAT**
Напечатать первую не пустую строку
```
repeat
    line = io.read()
until line ~= ""
print(line)
```
В отличие от многих языков, в Луа в область действия локальных переменных входит условие цикла
Числовой оператор **FOR**
Числовой оператор FOR имеет следующий вид:
```
for var = exp1, exp2, exp3 do
        <something>
end
```Этот цикл будет выполнять `something` для каждого значения `var` от `exp1` до `exp2`, используя `exp3` как шаг для увеличения `var`.
Это третье выражение `exp3` необязательно; когда оно отстутствует, то Луа в качестве шага использует `1`.
```
function f(x)
    return x^2
end

for i=1, f(3) do print(i) end

for i = 10, 1, -1 do print(i) end
```
Если вы хотите получить цикл без верхнего предела, то вы можете использовать константу `math.huge`:
```
for i = 1, math.huge do
    if(0.3*i^3 - 20*i^2 - 500 >= 0) then
        print(i)
        break
    end
end

print(math.huge)    --> 1.#INF
```
Оператор **FOR** общего вида
напечатать все значения в таблице 't'
```
t = {a = "a",b = "b",c = "c",d = "d"}

for k, v in pairs(t) do print(k,v) end
```
Этот пример демонстрирует удобную итерирующую функцию для обхода всей таблицы, предоставленной базовой библиотекой Lua.
На каждом шаге этого цикла `k` получает индекс, а `v` - значение, связанное с этим индексом.
Несмотря на внешнюю простоту, оператор for общего вида очень мощная конструкция языка. С подходящими итераторами вы можете обойти практически все, что угодно, в легкочитаемой форме.
Стандартные библиотеки предоставляют несколько итераторов:
- io.lines      - перебор строк файла
- pairs         - пары из таблицы
- ipairs            - элементы последовательности
- string.gmatch     - слова внутри строки
Конечно, мы можем писать свои итераторы. Хотя использование оператора for в общей форме довольно легко, задача написания функции-итератора имеет свои тонкости. Мы рассмотрим эту тему позже.
Рассмотрим более конкретный пример использования оператора `for` общего вида:
Допустим у нас есть таблица с названиями дней недели:
```
days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}
```
Теперь вы хотите перевести название дня в его положение в неделе.
Вы можете обойти всю таблицу в поисках заданного имени.
Однако, как вы скоро узнаете, вам редко понадобиться искать в луа.
Более эффективным подходом, будет построение обратной таблицы, например revDays, в которой название дней является индексами, а значениями являются номера дней.
Эта таблица будет выглядеть следующим образом:
```
revDays = {
    ["Sunday"] = 1,
    ["Monday"] = 2,
    ["Tuesday"] = 3,
    ["Wednesday"] = 4,
    ["Thursday"] = 5,
    ["Friday"] = 6,
    ["Saturday"] = 7
}
```
Тогда все, что вам нужно для того, чтобы найти номер дня, это обратиться к этой обратной таблице:
```
x = "Tuesday"
print(revDays[x]) --> 3
```
Конечно, не нужно задавать явно эту обратную таблицу. Мы можем построить ее автоматически исходной:
```
revDays = {}

for k,v in pairs(days) do
    revDays[v] = k
end
```
Этот цикл выполнит присваивание для каждого элемента `days`, где переменная k получает ключ 1, 2, .. , а `v` получает значение "Sunday", "Monday" ...
```
x = "Tuesday"
print(revDays[x]) --> 3
```
**BREAK RETURN**
Оператор `break` понятен: прерывание действий в циклах.
Но в Луа, им можно пользоваться, и как выход из функций, если вы например, ни хотите ничего возвращать.
Иногда полезно использовать `RETURN` в целях отладки. Например, вы хотите избежать выполнение функции `foo()`:
```
function foo()
    do return end
    --  ....
    print("FOO")
end

foo()
```
## 5. Функции ##
### Функции ###
Существует специальное соглашение:
Если у функции всего один аргумент, либо литерал, либо конструктор таблицы, то мы можем записать функцию без скобок.
```
print "Hello world!"

--dofile 'a.lua'

print(type {})

function f(t)
    print(t.x,t.y)
end

f {x=10, y=20}
```
Луа также предлагает специальный синтаксис для ОО вызовов, оператор двоеточие.
Выражение вроде o:foo(x)  - это просто способ записать o.foo(o,x), то есть позвать o.foo, добавляя o как дополнительный аргумент. Это все подробнее мы обсудим позже
Общий синтаксис функции интуитивно понятен:
- ключевое слово function
- имя функции
- список агрументов
- тело функции
В Луа число аргументов не обязательно должно совпадать при вызове функции:
```
function foo(a,b) print(a,b) end
```
Функция обладает таким же поведением как и множественное присваивание
```
foo(3)          --> 3 nil
foo(3,4)        --> 3 4
foo(3,4,5)      --> 3 4 (5 отбрасывается)
```
Конечно, это свойство часто приводит к ошибкам в рантайме.
Но это свойство полезно при реализации функций с аргументами по умолчанию
Реализуем счетчик
```
count = 0

function incCount(n)
    n = n or 1
    count = count + n
end

incCount()
print(count)    --> 1
incCount(2)
print(count)    --> 3
```
* * *
### Множественные результаты ###
Мало распространенной, но тем не менее очень удобной особенностью Lua является то, что функция может возвращать несколько значений.
Возьмем пример функции `find`, которая возвращает индекс начала шаблона в строке и индекс конца шаблона:
```
s, e = string.find("hello Lua users!", "Lua")
print(s,e)  -->7 9 (обратите внимание, что индекс первого символа равен 1)
```
Вот например реализация функции возвращающей максимальный элемент последовательности и его индекс:
```
function maximum(a)
    local mi = 1
    local m = a[mi]
    for i = 1, #a do
        if a[i] > m then
            mi = i; m = a[i]
        end
    end
    return m,mi
end

print(maximum {8,10,23,12,4})   --> 23 3
```
Рассмотрим этот вопрос подробнее на примере нескольких функций:
```
function foo0() end                     -- ничего не возвращает
function foo1() return "a" end          -- возвращает 1 значение
function foo2() return "a","b" end      -- возвращает 2 значение

x,y = foo2()        -- x="a", y="b"
x = foo2()          -- x="a", "b" - отбрасывается
x,y,z = 10,foo2()   -- x=10,y="a",z="b"

x,y = foo0()        -- x=nil, y=nil
x,y = foo1()        -- x="a", y=nil
x,y,z = foo2()      -- x="a",y="b",z=nil
```
Вызов функции, который не является последним элементом в списке, дает ровно 1 значение
```
x,y = foo2(), 20        --x="a", y=20
x,y = foo0(), 20,30     --x=nil, y=20, 30 - отбрасывается


print(foo0())           -->
print(foo1())           --> a
print(foo2())           --> a b
print(foo2(),1)         --> a, 1
print(foo2() .. "x")    --> ax

t = {foo0()}            -- t = {} пустая таблица
t = {foo1()}            -- t = {"a"}
t = {foo2()}            -- t = {"a","b"}
```
Но
```
t = {foo0(), foo2(), 4} -- t[1] = nil, t[2] = "a", t[3] = 4
```
Наконец, оператор `return f()` возвращает все значения, которые вернула f:
```
function foo(i)
    if i == 0 then return foo0()
    elseif i == 1 then return foo1()
    elseif i == 2 then return foo2()
    end
end

print(foo(1))           --> a
print(foo(2))           --> a b
print(foo(0))           -- (нет значений)
print(foo(3))           -- (нет значений)
```
Вы можете "заставить" вызов вернуть только одно значение, заключив его в дополнительную пару круглых скобок:
```
print((foo0()))         --> nil
print((foo1()))         --> a
print((foo2()))         --> a
```
Будьте с этими скобками осторожнее.
* * *
### Функции с переменным числом аргументов ###
Следующая функция возвращает сумму всех свох аргументов
```
function add(...)
    local s = 0
    for i, v in ipairs{...} do
        s = s + v
    end
    return s
end

print(add(3,4,10,25,12)) -->54
```
Следующая функция, просто возвращает все переданные в нее аргументы
```
function id(...)
    return ...
end
print(id(1,2,3,4,5))    -->1 2 3 4 5
```
Следующая функция ведет себя также, но перед выводом печатает сообщение:
```
function foo1(...)
    print("calling foo:", ...)
    return id(...)
end

foo1(1,2,3,4,5,6,7)
```
Полезный пример:
Луа предоставляет отдельные функции для форматирования текста`string.format` и для записи текста `io.write`. Довольно просто объеденить эти две функции  одну с переменным числом аргументов:
```
function fwrite(fmt, ...)
    return io.write(string.format(fmt, ...))
end

fwrite("a")
fwrite("%d%d",4,5)
```
* * *
### Именованные аргументы ###
Довольно часто мы забываем, какое имя идет первым, новое или старое
```
--rename(old="temp.lua",new="temp1.lua")
```
Идея заключается в том, чтобы собрать все аргументы в таблицу как единственный аргумент функции. Специальный синтаксис, который Луа предоставляет для вызова функции, с конструктором таблицы как единственным аргументом, поможет нам добиться этого:
```
--rename {old="temp.lua", new="temp1.lua"}
```
Cоответственно, мы переопределяем функцию rename только с одним параметром и получаем настоящие аргументы из этого параметра
```
function rename(arg)
    return os.rename(arg.old, arg.new)
end


rename {old="temp.lua", new="temp1.lua"}
```
Если осознать эту идеалогию можно совершенно иначе и с другого угла взглянуть на возможности аргументов у функций в Луа.
Как вы уже видите, возможности, казалось бы, простого механизма функций в Луа очень велики. И это не предел. Впереди все самое интересное. Функции - одна из причин, называть Луа, одним из лучших скриптовых ЯП.
* * *
### Еще о функциях... ###
Функции в Lua являются значениями первого класса с соответствующей лексической областью действия.
Это значит, что Lua функция - это значение, обладающее теми же правами, что и стандартные значения при вводе чисел и строк. Мы можем сохранять функции в переменных(локальных и глобальных) и в таблицах, мы можем передавать функции как аргументы и возвращать их из других функций.
Что значит для функций "Лексическая область действия"? Это значит, что функции могут обращаться к переменным, содержащим их функции. Как мы увидем в этом уроке, это вроде безобидное свойство дает огромную мощь языку, поскольку позволяет применять в Lua многие могущественные приемы из мира функционального программирования.
Несколько смущающим понятием в Луа является то, что функции, как и другие значения, являются анонимными; у них нет имен. Когда мы говорим об имени функции, например `print`, мы имеем в виду переменную, которая содержит эту функцию.
Следующий пример, хотя и несколько надуман, показывает возможные примеры:
```
a = {p = print}

a.p("hello world")  --> Hello world

print = math.sin    -- print теперь ссылается на синус

a.p(print(1))       --> 0.84170

sin = a.p           --> sin теперь ссылается на функцию print

sin(10,20)          --> 10 20
```
Позже мы увидем полезные применения этой возможности
Если функции являются значениями, то существуют ли выражения, которые создают функции?
Да, в частности, стандартный способ создать функцию в Lua, как например,
```
function foo(x) return 2*x end
```
Это просто пример того, что мы называем синтаксическим сахаром;
Это просто более красивый способ написать следующий код:
```
foo = function (x) return 2*x end;
```
Поэтому определение функции - это на самом деле оператор(присваивания),
который создает значение типа function и присваивает его переменной.
Мы можем рассматривать выражение function(x) body end как конструктор функции,
точно так же, как {} является конструктором таблицы. Мы называем результат выполнения подобных конструкторов анонимной функцией. Хотя мы часто присваиваем функции глобальным переменным, давая им что-то вроде имени, бывают случаи, когда функции остаются анонимными
Давайте рассмотрим несколько примеров:
Библиотека `table` предоставляет функцию table.sort, которая получает таблицу и сортирует ее элементы. Подобная функция должна предоставлять бесконечные вариации порядка сортировки: по возрастанию и по убыванию, числовой или по алфавиту, по какому-то ключу и т. д.
Вместо попытки предоставить все возможные опции `sort` предоставляет дополнительный параметр,
который является функцией упорядочения: функция, которая получает два аргумента и определяет, должен ли первый элемент идти перед вторым в отсортированном списке. 
Например, допустим, что у нас есть следующая таблица записей:
```
network = {
    {name = "grauna",   IP = "210.26.30.34"},
    {name = "arriaial", IP = "210.26.30.23"},
    {name = "lua",      IP = "210.26.23.12"},
    {name = "derain",   IP = "210.26.23.20"},
}
```
Если вы хотите отсортировать таблицу по полю name в обратном алфавитном порядке, то вы можете просто записать:
```
table.sort(network, function(a,b) return (a.name > b.name) end)

print(network[1].name)
```
Функция, которая возвращает другую функцию как агрумент, является тем, что мы называем функцией высшего порядка
Для того чтобы проиллюстрировать использование функций высших порядков, мы напишем упрощенное определение часто встречающейся функции высшего порядка, производной.
Следуя неформальному определению, производная функции `f` в точке x - это значение `(f(x+d)-f(x))/d`, когда `d` становится бесконечно малой.
В соответствии с этим определением, мы можем написать приближенное значение производной следующим образом:
```
function derivative (f, delta)

    delta = delta or 1e-4
    return function(x)
        return (f(x+delta) - f(x))/delta
    end
end
```
Получив функцию `f`, вызов `derivative(f)` вернет приближенное значение ее производной, которое является другой функцией:
```
c = derivative(math.sin)
print(math.cos(5.2),c(5.2))
--> 0.46851667130038    0.46856084325086

print(math.cos(10), c(10))
--> -0.83907152907645   -0.83904432662041
```
Настоятельно рекомендую понять весь этот принцип
Поскольку функции являются значениями первого класса в Луа, мы можем запоминать их не только в глобальных переменных, но и в локальных переменных и полях таблиц.
Как мы увидем дальше, использование функций в полях таблицы - это ключевой компонент некоторых продвинутых возможностей Луа, таких как модули и ООП...^^
* * *
### Замыкания ###
Когда мы пишем функцию, заключенную внутри другой функции, то она имеет полный доступ к локальным переменным окружающей ее функции. Мы называем это - лексической областью действия.
Начнем с простого примера.
Пусть у нас есть список имен и таблица, сопоставляющая
им оценки. Вы хотите отсортировать список студентов
по их оценкам.
Вы можете добиться этого следующим образом:
```
names = {"Peter","Paul","Mary"}
grades = {Mary = 10, Paul = 7, Peter = 8}

table.sort(names, function(n1,n2)
    return grades[n1] > grades[n2]  --Сравнить оценки
end)
```
Теперь допустим, что вы хотите создать функцию для решения данной задачи:
```
function sortbygrade (names,grades)
    table.sort(names, function(n1,n2)
    return grades[n1] > grades[n2]
    end)
end
```
Главной особенностью этого примера является то что анонимная функция передаваемая в функцию соритровки таблицы, обращается к параметру `grades`, который является локальным для заключающей их функции `sortbygrade`
Рассмотрим следующий пример
```
function newCounter ()
    local i = 0
    return function ()  --Анонимная функция
        i = i + 1
        return i
    end
end

c1 = newCounter()
print(c1())     --> 1
print(c1())     --> 2
```
Если мы снова позовем newCounter, то она создаст новую локальную переменную i, поэтому мы получим новое замыкание, работающее с этой новой переменной
```
c2 = newCounter()
print(c2())     --> 1
print(c1())     --> 3
print(c2())     --> 2
```
Таким образом, с1 и с2 - это разные замыкания одной и той же функции, где каждая использует свою независисмо интсалиционную переменную i
Замыкания также оказываются полезными в совсем другом случае. Поскольку функции хранятся в отдельных переменных, мы легко можем переопределять функции в Lua, включая даже стандартные. Эта возможность является одной из причин, почему
Lua столь гибок.
Часто, когда вы переопределяете функцию, вам все равно нужна старая функция. Например, вы хотите переопределить функцию sin, чтобы она работала с градусами вместо радиан. Эта новая функция преобразует свой аргумент и затем зовет исходную sin,для выполнения работы. Например:
```
oldSin = math.sin
math.sin = function (x)
    return oldSin(x*math.pi/180)
end
```
Далее приведен немного более аккуратный способ выполнить это переопределение:
```
do
    local oldSin = math.sin
    local k = math.pi/180
    math.sin = function(x)
        return oldSin(x*k)
    end
end
```
Теперь мы сохраняем старую версию в локальной переменной
Единственный способ обратиться к ней - через новую функцию
* * *
### Неглобальные функции ###
Т.к. функции являются значениями первого класса, то мы можем сохранять их не только в глобальных переменных, но также и в локальных и в полях таблицы
Создаются поля таблицы интуитивно понятно:
```
Lib = {}

Lib.plus = function (x, y) return x + y end
Lib.minus = function (x, y) return x - y end

print(Lib.plus(2,2), Lib.minus(2,2))    --> 4 0
```
Конечно, мы также можем использовать конструкторы
```
Lib = {

plus = function (x, y) return x + y end,
minus = function (x, y) return x - y end

}

print(Lib.plus(2,2), Lib.minus(2,2))    --> 4 0
```
Более того, Луа также предоставляет еще один синтаксис для подобных функций:
```
Lib = {}

function Lib.plus (x,y) return x+y end
function Lib.minus (x,y) return x-y end
```
Когда мы запоминаем функцию в локальной переменной мы получаем локальную функцию с ограниченной областью видимости.
```
local sum = function (a,b)
    return a+b
end

local super_sum = function(a,b)
    return sum(a,b) + (a+b)     --sum здесь видна
end

print(super_sum(4,4))   --> 16
```
Луа также поддерживает синтаксический сахар для локальных функций:
```
local function mul(a,b)
    return a^b
end

print(mul(2,10))    --> 1024
```
При определении рекурсивных функций, наивный подход здесь не работает
```
local fact = function(n)
    if n == 0 then return 1
    else return n*fact(n-1) -- ошибка
    end
end
```
Когда Луа компилирует вызов *fact(n-1) в теле функции, то локальная переменная fact еще не определена. Поэтому данное определение попытается позвать глобальную
функцию fact, а не локальную. Решается проблема просто:
```
local fact
fact = function(n)
    if n == 0 then return 1
    else return n*fact(n-1)
    end
end

print(fact(10)) -->3628800
```
Когда луа раскрывает свой синтаксический сахар для локальной функции, она не использует наивный путь. Вместо этого определения:
```
local function foo() end
```
переходит в
```
local foo; foo = function() end
```
* * *
### Оптимизация хвостовых вызовов ###
Хвостовой вызов случается когда функция вызывает другую функцию как последнее свое действие.
Например, в следующем коде вызов функции g является хвостовым:
```
function f(x) return g(x) end
```
После того, как f вызовет g, ей больше нечего делать.
Поэтому интерпретатор больше не возвращается f, а после завершения g, просто возвращается в место на стеке, где была вызвана f.
Поскольку хвостовые вызовы не используют место на стеке, количество вложенных хвостовых вызовов, которое программа может выполнить, просто ничем не ограничено. Например, мы можем вызвать следующую функцию, передав любое число в качестве аргумента:
```
function foo (n)
    if n > 0 then return foo(n-1) end
end
```
Этот вызов никогда не приведет к переполнению стека
Тема хвостовых вызовов, лишь доказывает то, что Луа очень умная, и старается всячески не делать лишних вызовов и старается не запоминать то, что не нужно.
## 6. Итераторы и обобщенный for ##
### Итераторы и замыкания ###
Итератор - это любая конструкция позволяющая вам перебирать элементы набора
В луа итераторы представляют собой функции, каждый раз, как мы ее вызываем, он возвращает следующий элемент набора
В качестве примера, давайте напишем простой итератор для списка.
В отличии от ipairs, этот итератор будет возвращать не индекс каждого элемента, а его значение
```
function values (t)
    local i = 0
    return function() i = i+1; return t[i] end
end
```
В этом примере `values` - это фабрика. Каждый раз, когда мы вызывам эту фабрику, она создает новое замыкание(итератор).
Это замыкание хранит свое состояние во внешних переменных `t` и `i` Каждый раз, когда мы вызываем этот итератор, он возвращает следующее значение из списка `t`. После последнего элемента итератор вернет `nil`, что обозначает конец итераций.
Мы можем использовать этот итератор в цикле `while`:
```
t = {10,20,30}
iter = values(t)            -- созздаем итератор
while true do
    local element = iter()  -- вызываем итератор
    if element == nil then break end
    print(element)
end
```
Однако гораздо проще использовать обобщенный итератор for.
В конце-концов, он был создан именно для подобного итерирования:
```
t = {1,2,3}
for element in values(t) do
    print(element)
end
```
Как видите, этот оператор for делает всю закулисную работу за нас
Ну и наконец приведем пример итератора, который обходит все слова в строке
```
function allwords(str)
    local pos = 1               -- текущая позиция в строке
    return function ()          -- функция итератор
        while true do
        local s, e = string.find(str, "%w+", pos)
            if s then           -- нашли слово?
                pos = e + 1     -- следущая позиция после слова
                return string.sub(str, s, e)    --вернуть слово
            end
        return nil              --больше нет слов, конец обхода
        end
    end
end


for word in allwords("i love you\n my world") do
    print(word)
end
--> i
--> love
--> you
--> my
--> world
```
* * *
### Семантика обобщенного FOR ###
Одним из недостатков рассмотренных выше итераторов является то, что нам необходимо создавать новое замыкание для инициализации каждого цикла. Для большинства случаев это не является проблемой. Однако в некоторых случаях это может оказаться существенным.
Мы видели, что обобщенный FOR во время цикла хранит итерирующую функцию внутри себя. На самом деле он хранит три значения:
1. итерирующую функцию
2. неизменяемое значение
3. управляющую переменную
Теперь давайте обратимся к деталям:
Синтаксис обобщенного FOR приводится ниже:
```
  for <var-list> in <exp-list> do
    <body>
  end
```
Здесь `var-list` - это список из одного или нескольких имен переменных, разделенных запятыми, а `exp-list` - это список из одного или нескольких выражений, также разделенных запятыми.
Часто список выражений состоит из единственного элемента, вызова фабрики итераторов.
В следующем коде, например, список переменных это `k`,`v`, а список выражений состоит из единственного элемента `pairs(t)`:
```
t = {1,2,3,4,5}
for k, v in pairs(t) do print(k,v) end
```
Часто список переменных также состоит всего из одной переменной как в след. цикле:
```
for line in io.lines() do
  io.write(line,"\n")
end
```
Мы вызываем первую переменную в списке управляющей переменной. В течение всего цикла ее значение не равно `nil`, поскольку когда она становится равно nil, цикл завершается.
Первое, что делает `for`, это вычисляет значения выражений, идущих после in.
Эти выражения должны дать три значения, используемые оператором for:
-итерирующая функция
-неизменяемое состояние
-начальное значение управляющей переменной.
После этой инициализации `for` вызывает итерирующую функцию с двумя аргументами:
-инвариантным состоянием
-управляющей переменной
(С точки зрения оператора `for`, это инвариантное состояние вообще не имеет никакого смысла. Оператор `for` только передает значение состояния с шага инициализации вызову итерирующей функции)
Затем `for` присваивает значения, возвращаемые итерирующей функцией,
переменным, объявленным в списке переменных.
Если первое значение равно `nil`, то цикл завершается. Иначе for выполнит
свое тело и снова зовет итерирующую функцию, повторяя процесс.
* * *
### Итераторы без состояния ###
Такой итератор не хранит в себе какого-либо состояния. Это нужно для того, чтобы не создавать новых замыканий. Типичным примером этого итератора является ipairs:
```
a = {"one","two","three"}

for i, v in ipairs(a) do
  print(i,v)
end
```
Состояние этого итератора - это таблица, которую мы перебираем.
Реализовали бы мы такой итератор примерно так:
```
local function iter(a, i)
  i = i + 1
  local v = a[i]
  if v then
    return i, v
  end
end

function ipairs(a)
  return iter, a, 0
end
```
## Используемая литература: ##
Roberto Ierusalimschy - Programming in Lua - 2014[RUS/ENG]